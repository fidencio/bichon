// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package cache

import (
	"gitlab.com/bichon-project/bichon/model"
)

type Cache interface {
	AddRepo(repo *model.Repo) error
	RemoveRepo(repo *model.Repo) error
	ListMergeRequests(repo *model.Repo) ([]uint, error)
	SaveMergeRequest(mreq *model.MergeReq) error
	LoadMergeRequest(repo *model.Repo, id uint) (*model.MergeReq, error)
}
