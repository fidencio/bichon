// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package main

import (
	"crypto/tls"
	"flag"
	"fmt"
	"math/rand"
	"os"
	"time"

	"github.com/spf13/pflag"
	"gitlab.com/bichon-project/bichon/controller"
	"gitlab.com/bichon-project/bichon/net"
	"gitlab.com/bichon-project/bichon/security"
	"gitlab.com/bichon-project/bichon/view"
)

func main() {
	var cacerts string
	var httplogdir string
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.CommandLine.StringVar(&cacerts, "ca-certs", "",
		"path to PEM file containing extra CA certificates to trust")
	pflag.CommandLine.StringVar(&httplogdir, "http-log-dir", "",
		"path to directory to store HTTP request/response logs")
	pflag.Parse()

	rand.Seed(time.Now().UnixNano())

	var tlscfg *tls.Config
	var err error
	if cacerts != "" {
		tlscfg, err = security.LoadCACerts(cacerts)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Unable to load CA certs from %s: %s\n", cacerts, err)
			return
		}
	}

	net.SetHTTPLogDir(httplogdir)

	engine, err := controller.NewEngine(tlscfg)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to initialize engine: %s\n", err)
		return
	}

	display, err := view.NewDisplay(engine)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to create display: %s\n", err)
		return
	}

	display.Run()
}
