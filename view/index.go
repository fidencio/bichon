// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"fmt"
	"strings"

	"github.com/gdamore/tcell"
	"github.com/golang/glog"
	"github.com/rivo/tview"

	"gitlab.com/bichon-project/bichon/model"
)

type IndexPageListener interface {
	IndexPageQuit()
	IndexPageViewMergeRequest(mreq model.MergeReq)
	IndexPageViewProjects()
	IndexPageRefreshMergeRequests()
	IndexPagePickSort()
	IndexPagePickFilter()
}

type IndexPage struct {
	*tview.Frame

	Application *tview.Application
	Listener    IndexPageListener
	MergeReqs   *tview.Table
}

func NewIndexPage(app *tview.Application, listener IndexPageListener) *IndexPage {
	mreqs := tview.NewTable().
		SetSelectable(true, false).
		SetSelectedStyle(
			GetStyleColor(ELEMENT_MREQS_ACTIVE_TEXT),
			GetStyleColor(ELEMENT_MREQS_ACTIVE_FILL),
			GetStyleAttrMask(ELEMENT_MREQS_ACTIVE_ATTR))

	layout := tview.NewFrame(mreqs).
		SetBorders(0, 0, 0, 0, 0, 0)

	page := &IndexPage{
		Frame: layout,

		Application: app,
		Listener:    listener,
		MergeReqs:   mreqs,
	}

	return page
}

func (page *IndexPage) GetName() string {
	return "index"
}

func (page *IndexPage) GetKeyShortcuts() string {
	return "q:Quit r:Refresh p:Projects s:Sort f:Filter ?:Help"
}

func (page *IndexPage) buildMergeReqRow(mreq *model.MergeReq) [8]*tview.TableCell {
	nversions := len(mreq.Versions)
	version := mreq.Versions[len(mreq.Versions)-1]
	npatches := len(version.Patches)

	status := mreq.Metadata.Status
	if status == model.STATUS_READ {
		status = ""
	}
	if len(status) > 0 {
		status = status[0:1]
	}

	return [8]*tview.TableCell{
		&tview.TableCell{
			Text:            strings.ToUpper(string(status)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Reference:       mreq,
			Align:           tview.AlignRight,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%5s", fmt.Sprintf("#%d", mreq.ID)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignRight,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%20s", TrimEllipsisFront(mreq.Repo.Project, 20)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignRight,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf(" %-8s", mreq.Age()),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%20s", TrimEllipsisFront(mreq.Submitter.RealName, 20)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%4s", fmt.Sprintf("v%d", nversions)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%-7s", fmt.Sprintf("0/%d", npatches)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            mreq.Title,
			Expansion:       1,
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
	}
}

func (page *IndexPage) refreshMain(app *tview.Application, mreqs []*model.MergeReq) {
	glog.Infof("Refreshing index page main")
	page.updateMergeReqs(mreqs)
}

func (page *IndexPage) Refresh(app *tview.Application, mreqs []*model.MergeReq) {
	glog.Infof("Refreshing index page queue")
	app.QueueUpdateDraw(func() {
		page.refreshMain(app, mreqs)
	})
}

func (page *IndexPage) Activate() {
	page.Application.SetFocus(page.MergeReqs)
}

func (page *IndexPage) HandleInput(event *tcell.EventKey) *tcell.EventKey {
	switch event.Key() {
	case tcell.KeyRune:
		switch event.Rune() {
		case 'q':
			page.Listener.IndexPageQuit()
		case 'r':
			page.Listener.IndexPageRefreshMergeRequests()

		case 'p':
			page.Listener.IndexPageViewProjects()
		case 's':
			page.Listener.IndexPagePickSort()
		case 'f':
			page.Listener.IndexPagePickFilter()
		default:
			return event
		}

	case tcell.KeyCR:
		mreq := page.GetSelectedMergeRequest()
		if mreq != nil {
			page.Listener.IndexPageViewMergeRequest(*mreq)
		}
	default:
		return event
	}

	return nil
}

func (page *IndexPage) GetSelectedMergeRequest() *model.MergeReq {
	if page.MergeReqs.GetRowCount() == 0 {
		return nil
	}
	row, _ := page.MergeReqs.GetSelection()
	glog.Infof("Row selected %d", row)
	cell := page.MergeReqs.GetCell(row, 0)

	ref := cell.GetReference()

	if ref == nil {
		return nil
	}

	mreq, ok := ref.(*model.MergeReq)
	if !ok {
		return nil
	}
	glog.Infof("Cell selected mreq %p %d:%s", mreq, mreq.ID, mreq.Title)
	return mreq
}

func (page *IndexPage) updateMergeReqs(mreqs []*model.MergeReq) {
	cells := make([][8]*tview.TableCell, 0)
	for _, mreq := range mreqs {
		cells = append(cells, page.buildMergeReqRow(mreq))
	}

	page.MergeReqs.Clear()
	for idx, row := range cells {
		for col, _ := range row {
			page.MergeReqs.SetCell(idx, col, row[col])
		}
	}
}
